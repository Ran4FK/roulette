<?php

require_once('model/database.php');

class User extends Database {

    public function __construct() {
        parent::__construct();
    }

    public function login($name, $password) {
        $connection = $this->dbConnect();
        $query = $connection->prepare("SELECT id FROM user WHERE (login = ? OR mail = ?) AND password = ?");
        $query->execute([$name, $name, $password]);
        return $query->fetch();
    }

    public function register($name, $email, $password) {
        $connection = $this->dbConnect();
        $query = $connection->prepare("INSERT INTO `user` (`login`, `mail`, `password`) VALUES (?, ?, ?)");
        $query->execute([
            $name,
            $email,
            password_hash($password, PASSWORD_BCRYPT, ["cost" => 12])
        ]);
        return $connection->lastInsertId();
    }
}

