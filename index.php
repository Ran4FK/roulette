<?php
session_start();

date_default_timezone_set("Europe/Paris");

require_once('./controller/users.php');

$user = new Users();

if(!empty($_GET['action'])) {
    switch($_GET['action']) {
        case 'register':
            $user->register();
            break;
        default: 
            $user->login();
            break;

    }
}
else {
    $user->login();
}