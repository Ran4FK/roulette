CREATE TABLE roulette.user (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT ,
    login VARCHAR(30) NOT NULL ,
    mail VARCHAR(200) NOT NULL ,
    password VARCHAR(255) NOT NULL ,
    dt_creation DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    dt_last_login DATETIME NULL ,
    PRIMARY KEY (id),
    UNIQUE (login),
    UNIQUE (mail)
) ENGINE = InnoDB;