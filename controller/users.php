<?php

require_once('model/user.php');

class Users {

    private User $user;

    public function __construct() {
        $this->user = new User();
        $_SESSION=[];
    }

    public function login() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return require('view/login.php');
        }

        $name = empty(trim($_POST['name']));
        $password = empty(trim($_POST['password']));
        if ($name || $password) {
            $errName = $name ? 'Introduire un pseudo ou un email' : '';
            $errPassword = $password ? 'Introduire un mot de passe' : '';
            return require('view/login.php');
        }
        $id = $this->user->login($_POST['name'], $_POST['password']);
        if($id === false) {
            $_SESSION['errlogin'] = 'Mauvais identifiant / mot de passe';
            return require('view/login.php');
        }
        $_SESSION['login'] = 'Connecté';
        require('view/profil.php');
    }
    public function register() {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            return require('view/register.php');
        }
        $name = empty(trim($_POST['name']));
        $email = empty(trim($_POST['email']));
        $password = empty(trim($_POST['password']));
        $repeatPassword = empty(trim($_POST['repeatPassword']));
        if ($name || $email || $password || $repeatPassword) {
            $field = 'Le champ est vide !';
            $errName = $name ? $field : '';
            $errEmail = $email ? $field : '';
            $errPassword = $password ? $field : '';
            $errRepeatPassword = $repeatPassword ? $field : '';
            return require('view/register.php');
        }
        if($_POST['repeatPassword']) {
            $errRepeatPassword = 'Les mots de passe ne correspondent pas !';
            return require('view/register.php');
        }
        $this->user->register($_POST['name'], $_POST['email'], $_POST['password']);

        $_POST;
        $_SESSION['SuccedAccount'] = 'Le compte a bien été crée, vous pouvez vous connecter';
        header('Location: ?action=login');
    } 
}

