<?php $title = 'Sign in'; ?>

<?php $css = ["middle-form"]; ?>

<?php ob_start(); ?>

<!-- <a href="index.php?action=login">Connexion</a> -->

<div class="middle-form">
    <!-- <h3 class="mt-5 fw-normal">Sign in</h3> -->
    <form method="post">
        <?php if (!empty($errName)) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;<?= $errName ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif ?>
        <div class="form-floating">
        <input type="text" class="form-control" id="name" name="name" value="<?= !empty($_POST['name']) ? $_POST['name'] : ''?>" > 
        <label for="name">Pseudo</label>

        <?php if (!empty($errEmail)) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;<?= $errEmail ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif ?>
        <div class="form-floating">
        <input type="email" class="form-control" id="email" name="email" value="<?= !empty($_POST['email']) ? $_POST['email'] : ''?>" >
        <label for="email">Email</label>

        <?php if (!empty($errPassword)) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;<?= $errPassword ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif ?>
        <div class="form-floating">
        <input type="password" class="form-control" id="password" name="password">
        <label for="password">Password</label>

        <?php if (!empty($errRepeatPassword)) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;<?= $errRepeatPassword ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif ?>
        <div class="form-floating">
        <input type="password" class="form-control" id="repeatPassword" name="repeatPassword">
        <label for="repeatPassword">Repeat password</label>
        <br>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Confirm</button>
        <br>
    </form>
</div>

<?php $content = ob_get_clean(); ?>

<?php require('templates/template.php'); ?>