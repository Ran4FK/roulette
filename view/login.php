
<?php $title = 'Login'; ?>

<?php $css = ["middle-form"]; ?>

<?php ob_start(); ?>




<div class="middle-form mt-5">
    <!-- <h3 class="mt-5 fw-normal">Please connect</h3> -->
    <form method="post">
        <?= !empty($_SESSION['SuccedAccount']) ? $_SESSION['SuccedAccount'] : '' ?>
        <?php if (!empty($errName)) : ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;<?= $errName ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif ?>
        <div class="form-floating">
            <input type= "text" class="form-control" id="name" name="name" value="<?= !empty($_POST['name']) ? $_POST['name'] : ''?>" />
            <label for="name">Login or mail</label>
        </div>
        <?php if (!empty($errPassword)) : ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <i class="fa-solid fa-circle-exclamation"></i>&nbsp;<?= $errPassword ?>
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
        <?php endif ?>
        <div class="form-floating">
            <input type= "password" class="form-control" id="password" name="password" >
            <label for="password">Password</label>
        </div>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
        <br/>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Confirm</button>
    </form>
</div>


<?php $content = ob_get_clean(); ?>

<?php require('templates/template.php'); ?>