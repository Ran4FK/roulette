<!DOCTYPE html>
<html class="h-100">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title><?= $title ?></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <script type="text/javascript" src="public/js/script.js"></script>
        <link rel="stylesheet" media="screen" type="text/css" href="public/css/style.css"/>
    </head>
    <?php if(!empty($js)): ?>
        <?php foreach($js as $script): ?> 
            <script type="text/javascript" src="public/js/<?= $script ?>.js"></script>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if(!empty($css)): ?>
        <?php foreach($css as $script): ?> 
            <link rel="stylesheet" media="screen" type="text/css" href="public/css/<?= $script ?>.css"/>
        <?php endforeach; ?>
    <?php endif; ?>

    <body class="d-flex flex-column h-100">

        <?php require('menu.php'); ?>

        <main class="d-flex flex-column h-100">
            <div class="d-flex flex-column container">
                <?php require('header.php'); ?>
                <?= $content ?>
            </div>
        </main>

        <?php require('footer.php'); ?>
    </body>
</html>